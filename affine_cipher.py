# todo: restrictions for a and b
# from rec_affine_cipher import get_text


def get_text(filename):
    with open(f'{filename}', 'r') as f:
        text = f.read()
    return text


m = 33
# coprime_numbers = list(filter(lambda i: i not in [3, 6, 9, 11, 12, 15, 18], range(33)))   # not coprime numbers are 3 and 11
coprime_numbers = list(filter(lambda i: i % 3 != 0 and i % 11 != 0, range(33)))
rus_alph = list('абвгдеёжзийклмнопрстуфхцчшщъыьэюя')


# rus_alph = [
#     chr(letter) for letter in range(1072, 1104) # ё - 1105
# ]
# m = 32  # w/o  ё


# расширен. алг. Евклида
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def mod_inverse(a, m=m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m


####################################### через алфавит с ё #################################################

def encrypt_message(a, b, message):
    message = message.lower()
    ''' 
        C = (a*P + b) % m 
    '''
    encrypted_message = []
    for letter in message:
        if letter not in rus_alph:  # знаки преп
            encrypted_message.append(letter)
            continue
        letter_number = (a * rus_alph.index(letter) + b) % m  # in rus_alph w/o ё 1072 - ord('а')
        encrypted_message.append(rus_alph[letter_number])
    return ''.join(encrypted_message)


def decrypt_message(a, b, message):
    message = message.lower()
    ''' 
        P = (a^-1 * (C - b)) % m 
    '''
    try:
        inv_a = mod_inverse(a, m)
        # inv_a = 13
        decrypted_message = []
        for letter in message:
            if letter not in rus_alph:
                decrypted_message.append(letter)
                continue
            letter_number = (inv_a * (rus_alph.index(letter) - b)) % m  # in rus_alph w/o ё
            new_letter = rus_alph[letter_number]
            decrypted_message.append(new_letter)
    except Exception as e:  # qw
        print(str(e))

    return ''.join(decrypted_message)


####################################### через алфавит без ё #################################################

# def encrypt_message(a, b, message):
#     message = message.lower()
#     '''
#         C = (a*P + b) % m
#     '''
#     encrypted_message = []
#     for letter in message:
#         if letter == 'ё':
#             letter = 'е'
#         if letter not in rus_alph:  # знаки преп
#             encrypted_message.append(letter)
#             continue
#         letter_number = (a * (ord(letter) - 1072) + b) % m  # in rus_alph w/o ё 1072 - ord('а')
#         encrypted_message.append(chr(letter_number + 1072))
#     return ''.join(encrypted_message)
#
#
# def decrypt_message(a, b, message):
#     message = message.lower()
#     '''
#         P = (a^-1 * (C - b)) % m
#     '''
#     try:
#         inv_a = mod_inverse(a, m)
#         # inv_a = 13
#         decrypted_message = []
#         for letter in message:
#             # if new_letter == 'ё':  # qw
#             #     new_letter = 'е'
#             if letter not in rus_alph:
#                 decrypted_message.append(letter)
#                 continue
#             letter_number = (inv_a * ((ord(letter) - 1072) - b)) % m  # in rus_alph w/o ё
#             new_letter = chr(letter_number + 1072)
#             decrypted_message.append(new_letter)
#     except Exception as e:  # qw
#         print(str(e))
#
#     return ''.join(decrypted_message)

# todo: check eng/rus
# TODO: EXCEPTIONS!

if __name__ == '__main__':
    # print(coprime_numbers)
    while True:
        try:
            answer = (input('Choose your option: encrypt(1) or decrypt(2) for russian language: '))
            if answer not in ['1', '2']:
                print('Try again')
                continue

            a, b = map(int, input('Enter the key (a b): ').split(' '))
            if a not in coprime_numbers:
                raise Exception

            text = input('Enter the text: ')
            if answer == '1':
                # text = get_text('test_text.txt')
                # print(text)
                em = encrypt_message(a, b, text)
                print('Encrypted text: ', em)
                with open('encrypted.txt', 'w') as f:
                    f.write(em)
            elif answer == '2':
                # text = get_text('encrypted.txt')
                # print(text)
                print('Decrypted text: ', decrypt_message(a, b, text))

        except ValueError:
            print('Wrong key, try again')  # qw or sth went wrong
        except Exception:
            print(f'Your a in the key must be a coprime number with m = {m}')
            # print(f'Your a in the key must be a coprime number with {m=}')    # python 3.8 version


        # text = input('enter text:')
        # a, b = map(int, input('enter key: ').split(' '))
        # new_text = encrypt_message(a, b, text)
        # print('encryption:   ', new_text)
        # print('decryption:   ', decrypt_message(a, b, new_text))
