from affine_cipher import mod_inverse, rus_alph, m, coprime_numbers


# m = 33
# rus_alph = list('абвгдеёжзийклмнопрстуфхцчшщъыьэюя')

####################################### через алфавит с ё #################################################
def encrypt_message(a1, b1, a2, b2, message):
    encrypted_message = []
    message = message.lower()
    a_arr = [a1, a2]
    b_arr = [b1, b2]
    for letter, a, b in zip(message[:2], a_arr, b_arr):
        if letter not in rus_alph:
            encrypted_message.append(letter)
            continue

        letter_number = (a * rus_alph.index(letter) + b) % m  # in rus_alph w/o ё 1072 - ord('а')
        encrypted_message.append(rus_alph[letter_number])

    for letter in message[2:]:
        if letter not in rus_alph:
            encrypted_message.append(letter)
            continue

        a_arr.append((a_arr[-1] * a_arr[-2]) % m)
        b_arr.append((b_arr[-1] + b_arr[-2]) % m)

        letter_number = (a_arr[-1] * rus_alph.index(letter) + b_arr[-1]) % m
        encrypted_message.append(rus_alph[letter_number])

    return ''.join(encrypted_message)


def decrypt_message(a1, b1, a2, b2, message):
    message = message.lower()
    decrypted_message = []
    inv_a_arr = [mod_inverse(a1, m), mod_inverse(a2, m)]
    b_arr = [b1, b2]
    for letter, i in zip(message[:2], range(2)):    # qw can be a mistake because of continue - if not letter
        if letter not in rus_alph:
            decrypted_message.append(letter)
            continue
        # for i in range(2):
        decrypted_message.append(rus_alph[(inv_a_arr[i] * (rus_alph.index(letter) - b_arr[i]) % m)])

    for letter in message[2:]:
        # if letter == 'ё':     # qw
        #     letter = 'е'
        if letter not in rus_alph:
            decrypted_message.append(letter)
            continue

        '''
        inv_a[i] = inv_a[i - 1] * inv_a[i - 2] 
        '''

        inv_a_arr.append((inv_a_arr[-1] * inv_a_arr[-2]) % m)
        b_arr.append((b_arr[-1] + b_arr[-2]) % m)

        inv_a = inv_a_arr[-1]
        b = b_arr[-1]

        # ord_rus = ord(letter) - 1072
        # letter_number = inv_a_arr[-1] * (ord_rus - b_arr[-1]) % m
        # new_letter = chr(letter_number + 1072)
        # decrypted_message.append(new_letter)

        letter_number = (inv_a_arr[-1] * (rus_alph.index(letter) - b_arr[-1])) % m  # in rus_alph w/o ё
        decrypted_message.append(rus_alph[letter_number])



    return ''.join(decrypted_message)


####################################### через алфавит без ё #################################################

# def encrypt_message(a1, b1, a2, b2, message):
#     encrypted_message = []
#     message = message.lower()
#     a_arr = [a1, a2]
#     b_arr = [b1, b2]
#     for letter, a, b in zip(message[:2], a_arr, b_arr):
#         if letter == 'ё':
#             letter = 'е'
#         if letter not in rus_alph:
#             encrypted_message.append(letter)
#             continue
#
#         letter_number = (a * (ord(letter) - 1072) + b) % m  # in rus_alph w/o ё 1072 - ord('а')
#         encrypted_message.append(chr(letter_number + 1072))
#
#     for letter in message[2:]:
#         if letter == 'ё':
#             letter = 'е'
#         if letter not in rus_alph:
#             encrypted_message.append(letter)
#             continue
#
#         a_arr.append((a_arr[-1] * a_arr[-2]) % m)
#         b_arr.append((b_arr[-1] + b_arr[-2]) % m)
#
#         letter_number = (a_arr[-1] * (ord(letter) - 1072) + b_arr[-1]) % m
#         new_letter = chr(letter_number + 1072)
#         encrypted_message.append(new_letter)
#
#     return ''.join(encrypted_message)
#
#
# def decrypt_message(a1, b1, a2, b2, message):
#     message = message.lower()
#     decrypted_message = []
#     inv_a_arr = [mod_inverse(a1, m), mod_inverse(a2, m)]
#     b_arr = [b1, b2]
#     for letter, i in zip(message[:2], range(2)):    # qw can be a mistake because of continue - if not letter
#         if letter == 'ё':
#             letter = 'е'
#         if letter not in rus_alph:
#             decrypted_message.append(letter)
#             continue
#         # for i in range(2):
#         decrypted_message.append(chr((inv_a_arr[i] * ((ord(message[i]) - 1072) - b_arr[i]) % m) + 1072))
#
#     for letter in message[2:]:
#         # if letter == 'ё':     # qw
#         #     letter = 'е'
#         if letter not in rus_alph:
#             decrypted_message.append(letter)
#             continue
#
#         '''
#         inv_a[i] = inv_a[i - 1] * inv_a[i - 2]
#         '''
#
#         inv_a_arr.append((inv_a_arr[-1] * inv_a_arr[-2]) % m)
#         b_arr.append((b_arr[-1] + b_arr[-2]) % m)
#
#         inv_a = inv_a_arr[-1]
#         b = b_arr[-1]
#
#         # ord_rus = ord(letter) - 1072
#         # letter_number = inv_a_arr[-1] * (ord_rus - b_arr[-1]) % m
#         # new_letter = chr(letter_number + 1072)
#         # decrypted_message.append(new_letter)
#
#
#         letter_number = (inv_a_arr[-1] * ((ord(letter) - 1072) - b_arr[-1])) % m  # in rus_alph w/o ё
#         new_letter = chr(letter_number + 1072)
#         decrypted_message.append(new_letter)
#
#
#
#     return ''.join(decrypted_message)


def get_text(filename):
    with open(f'{filename}', 'r') as f:
        text = f.read()
    return text




if __name__ == '__main__':
    # print(rus_alph)

    while True:
        try:
            answer = (input('Choose your option: encrypt(1) or decrypt(2) for russian language: '))
            if answer not in ['1', '2']:
                print('Try again')
                continue

            a1, b1, a2, b2 = map(int, input('Enter the key (a1 b1 a2 b2): ').split(' '))
            if a1 not in coprime_numbers or a2 not in coprime_numbers:
                raise Exception

            text = input('Enter the text: ')

            if answer == '1':
                # text = get_text('test_text.txt')
                # print(text)

                em = encrypt_message(a1, b1, a2, b2, text)
                print('Encrypted text: ', em)
                with open('encrypted.txt', 'w') as f:
                    f.write(em)
            elif answer == '2':
                # text = get_text('encrypted.txt')
                # print(text)

                dm = decrypt_message(a1, b1, a2, b2, text)
                print('Decrypted text: ', dm)
                with open('decrypted.txt', 'w') as f:
                    f.write(dm)

        except ValueError:
            print('Wrong key, try again')  # qw or sth went wrong
        except Exception:
            print(f'Your a in the key must be a coprime number with m = {m}')

